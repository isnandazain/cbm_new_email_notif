import sys
import time
import json
import logging
import datetime

import traceback
import htmlmin
import secrets
import numpy as np
import queue
import concurrent.futures

import helper.base as bs
import helper.database as db
import helper.service as svc
import helper.validation as validate

import libs.formatter as fmt

from config.constant import EmailnotifConfig


# Create Invoice
def process_actual_createinvoice(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        if payment_method not in ["DF", "VF"]:
            return None

        fitur_id = EmailnotifConfig.CREATE_INVOICE
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Invoice Created " + invoice_id

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs, invoice_vf = None, None

        if payment_method == "DF":
            invoice_mcs = invoice.detail_mcs[0]
            template_dir = 'resources/email_template/df/create_invoice_template.html'

        else:
            invoice_vf = invoice.detail_vf[0]
            template_dir = 'resources/email_template/vf/create_invoice_template.html'

        # terkait seller dan buyer name
        amount = fmt.int_to_money(amount=float(invoice.amount))
        seller = invoice.entitas_partner_name if payment_method == "VF" else invoice.entitas_anchor_name
        buyer = invoice.entitas_anchor_name if payment_method == "VF" else invoice.entitas_partner_name

        # get user
        users = []
        users_anchor = db.get_email_entitas(entitas_id=invoice.entitas_anchor, payment_method=invoice.payment_method, get_from="anchor")
        users_anchor = users_anchor[0].email_anchor.split("|") if users_anchor else []
        
        users_partner = db.get_email_entitas(entitas_id=invoice.entitas_anchor, partner_id=invoice.entitas_partner, payment_method=invoice.payment_method, get_from="partner")
        users_partner = users_partner[0].email_partner.split("|") if users_partner else []

        users = users + users_anchor + users_partner

        # check approval parent - anchor
        entitas_parent_anchor = db.get_detail_entitas(entitas_id=invoice.entitas_anchor)
        approval_parent_anchor = db.get_approval_ongoing(invoice_id=invoice_id, entitas_id=entitas_parent_anchor.parent_id, is_valid=True)
        if approval_parent_anchor:
            users_parent_anchor = db.get_email_entitas(entitas_id=entitas_parent_anchor.entitas_id, payment_method=payment_method, get_from="anchor")
            users_parent_anchor = users_parent_anchor[0].email_anchor.split("|") if users_parent_anchor else []
            users = users + users_parent_anchor

        # check approval parent - partner
        entitas_parent_partner = db.get_detail_entitas(entitas_id=invoice.entitas_partner)
        approval_parent_partner = db.get_approval_ongoing(invoice_id=invoice_id, entitas_id=entitas_parent_partner.parent_id, is_valid=True)
        if approval_parent_partner:
            users_parent_partner = db.get_email_entitas(entitas_id=entitas_parent_partner.entitas_id, payment_method=payment_method, get_from="anchor")
            users_parent_partner = users_parent_partner[0].email_anchor.split("|") if users_parent_partner else []
            users = users + users_parent_partner

        list_data_log = []
        for user in users:
            if payment_method == "DF":
                email_params = {
                    "email": user,
                    "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
                    "payment_method": invoice.payment_method,
                    "invoice_id": invoice_id,
                    "seller": seller,
                    "buyer": buyer,
                    "loan_account": invoice.account_disbursement,
                    "account_receivable": invoice.account_receivable,
                    "amount": amount,
                    "disburse_date": invoice.payment_date.strftime("%d-%m-%Y")
                }


            elif payment_method == "VF":
                vf_sharing_type = "Seller" if invoice.interest_sharing_type == "P" else "Buyer"
                int_vendor = fmt.int_to_money(amount=float(invoice_vf.interest_vendor))
                int_anchor = fmt.int_to_money(amount=float(invoice_vf.interest_anchor))
                prepaid_int = fmt.int_to_money(amount=float(invoice_vf.prepaid_interest))
                nett_amount = fmt.int_to_money(amount=float(invoice_vf.disbursement_amount_K2))

                email_params = {
                    "email": user,
                    "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
                    "payment_method": invoice.payment_method,
                    "invoice_id": invoice_id,
                    "seller": seller,
                    "buyer": buyer,
                    "loan_vendor": invoice_vf.loan_vendor,
                    "escrow_vendor": invoice_vf.esc_vendor,
                    "ops_vendor": invoice_vf.ops_vendor,
                    "amount": amount,
                    "vf_sharing_type": vf_sharing_type,
                    "sharing_limit_date": invoice.sharing_date.strftime("%d-%m-%Y"),
                    "interest_vendor": str(int_vendor),
                    "interest_anchor": str(int_anchor),
                    "prepaid_interest": str(prepaid_int),
                    "disbursement_amount_D": str(nett_amount),
                    "disburse_date": invoice.payment_date.strftime("%d-%m-%Y")
                }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)
        
            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": user,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=user,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)

        
        # generate request ke bri-notification
        logging.info("Send Email - Create Invoice : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                # start a future for a thread
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))
                        else:
                            response_service[index] = data

                        del request_data_collection[future]


        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )


        
    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))


def process_approval_invoice(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        if payment_method not in ["DF", "VF"]:
            return None

        fitur_id = EmailnotifConfig.APPROVE_INVOICE
        trx_type = "Approve Invoice"
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Pending Transaction " + invoice_id
        template_dir = 'resources/email_template/make_invoice_template.html'

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = invoice.detail_mcs[0]
        # terkait approval
        approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id, multiple_criteria=["approve", "confirm"], is_valid=True)
        if not approval_ongoing:
            approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id, multiple_criteria=["approve parent", "confirm parent"], is_valid=True)

        amount = fmt.int_to_money(amount=float(invoice.amount))
        seller = invoice.entitas_partner_name if payment_method == "VF" else invoice.entitas_anchor_name
        buyer = invoice.entitas_anchor_name if payment_method == "VF" else invoice.entitas_partner_name

        # validasi next role
        checkers = validate.validate_approve_invoice(
            user_id=user_id,
            invoice=invoice,
            approval_ongoing=approval_ongoing
        )

        users = [checker.detail_user.email for checker in checkers]
        list_data_log = []
        for user in users:
            email_params = {
                "event_trx": trx_type,
                "invoice_id": invoice_id,
                "reference_id": invoice.entitas_inv_no,
                "amount": amount,
                "trx_type": trx_type,
                "payment_method": payment_method,
                "payment_date": invoice.payment_date.strftime("%d-%m-%Y"),
                "user_name": approval_ongoing[0].user_name,
                "submitted_at": approval_ongoing[0].approval_date.strftime("%d-%m-%Y")
            }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": user,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=user,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)

        
        # generate request ke bri-notification
        logging.info("Send Email - Approval Approve Invoice : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))

                        else:
                            response_service[index] = data
                        
                        del request_data_collection[future]

        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

        return None

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))
