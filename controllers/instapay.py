import sys
import json
import time
import logging
import datetime

import traceback
import htmlmin
import secrets
import numpy as np
import queue
import concurrent.futures

import helper.base as bs
import helper.database as db
import helper.service as svc
import helper.validation as validate

import libs.formatter as fmt

from config.constant import EmailnotifConfig

# Instapay hanya untuk DF
def process_actual_instapay(invoice_id: str = None, user_id : str = None, payment_method: str = None):
    try:
        if payment_method != "DF":
            return None
        
        fitur_id = EmailnotifConfig.ACTUAL_INSTAPAY
        trx_type = "Actual Instant Payment"
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Instant Payment " + invoice_id
        template_dir = "resources/email_template/actual_instapay_template.html"

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = invoice.detail_mcs[0]
        approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id, multiple_criteria=["instapay", "instapay parent"])

        amount = fmt.int_to_money(amount=float(invoice.amount))
        
        users = []
        users_anchor = db.get_email_entitas(entitas_id=invoice.entitas_anchor, payment_method=invoice.payment_method, get_from="anchor")
        users_anchor = users_anchor[0].email_anchor.split("|") if users_anchor else []
        users = users + users_anchor

        # checker parent
        entitas_parent_anchor = db.get_detail_entitas(entitas_id=invoice.entitas_anchor)
        approval_parent_anchor = db.get_approval_ongoing(invoice_id=invoice_id, entitas_id=entitas_parent_anchor.parent_id, is_valid=True)
        if approval_parent_anchor:
            users_parent_anchor = db.get_email_entitas(entitas_id=entitas_parent_anchor.entitas_id, payment_method=payment_method, get_from="anchor")
            users_parent_anchor = users_parent_anchor[0].email_anchor.split("|") if users_parent_anchor else []
            users = users + users_parent_anchor

        seller = invoice.entitas_anchor_name
        buyer = invoice.entitas_partner_name

        list_data_log = []
        for user in users:
            recipient = user
            email_params = {
                "event_trx": trx_type,
                "invoice_id": invoice_id,
                "reference_id": invoice.entitas_inv_no,
                "amount": amount,
                "trx_type": trx_type,
                "payment_method": payment_method,
                "disburse_date": invoice.payment_date.strftime("%d-%m-%Y"),
                "user_name": approval_ongoing[0].user_name,
                "submitted_at": approval_ongoing[0].approval_date.strftime("%d-%m-%Y"),
                "seller": seller,
                "buyer": buyer
            }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": recipient,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=recipient,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)


        # generate request ke bri-notification
        logging.info("Send Email - Actual Instapay : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                # start a future for a thread
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))
                        else:
                            response_service[index] = data

                        del request_data_collection[future]


        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data['id_email'],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

        return None

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))


def process_approve_instapay(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        if payment_method != "DF":
            return None

        fitur_id = EmailnotifConfig.APPROVE_INSTAPAY
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Pending Transaction " + str(invoice_id)
        template_dir = "resources/email_template/make_invoice_template.html"
        trx_type = "Instant Payment"

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = invoice.detail_mcs[0]
        
        # terkait approval
        approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id, criteria="instapay", is_valid=True)
        if not approval_ongoing:
            approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id, criteria="instapay parent", is_valid=True)

        amount = fmt.int_to_money(amount=float(invoice.amount))

        # validasi next role
        checkers = validate.validate_approve_instapay(
            user_id=user_id,
            invoice=invoice,
            approval_ongoing=approval_ongoing
        )

        users = [checker.detail_user.email for checker in checkers]
        list_data_log = []
        for user in users:
            email_params = {
                "event_trx": trx_type,
                "invoice_id": invoice_id,
                "reference": invoice.entitas_inv_no,
                "amount": amount,
                "trx_type": trx_type,
                "payment_method": payment_method,
                "payment_date": invoice.payment_method.strftime("%d-%m-%Y"),
                "user_name": approval_ongoing.user_name,
                "submitted_at": approval_ongoing.approval_date.strftime("%d-%m-%Y")
            }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": user,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%s")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=user,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)

        # generate request ke bri-notification
        logging.info("Send Email - Approval Instapay : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))

                        else:
                            response_service[index] = data
                        
                        del request_data_collection[future]

        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

        return None

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))

