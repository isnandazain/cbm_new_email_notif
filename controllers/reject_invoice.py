import sys
import time
import json
import logging
import datetime

import traceback
import htmlmin
import secrets
import numpy as np
import queue
import concurrent.futures

import helper.base as bs
import helper.database as db
import helper.service as svc
import helper.validation as validate

import libs.formatter as fmt

from config.constant import EmailnotifConfig


def process_reject_invoice(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        if payment_method not in ["DF", "VF"]:
            return None

        fitur_id = EmailnotifConfig.REJECT_INVOICE
        trx_type = "Reject Invoice"
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Pending Transaction " + invoice_id
        template_dir = "resources/email_template/make_invoice_template.html"

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = invoice.detail_mcs[0]
        amount = fmt.int_to_money(amount=float(invoice.amount))
        approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id)

        user_already_approve = db.get_approval_ongoing(
            invoice_id=invoice_id,
            group_by_query="user_id"
        )

        users = [usr_approve.detail_user.email for usr_approve in user_already_approve]
        list_data_log = []
        for user in users:
            email_params = {
                "event_trx": trx_type,
                "invoice_id": invoice_id,
                "reference_id": invoice.entitas_inv_no,
                "amount": amount,
                "trx_type": trx_type,
                "payment_method": payment_method,
                "payment_date": invoice.payment_date.strftime("%d-%m-%Y"),
                "user_name": approval_ongoing.user_name,
                "submitted_at": approval_ongoing.approval_date.strftime("%d-%m-%Y")
            }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": recipient,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=recipient,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)
 
        # generate request ke bri-notification
        logging.info("Send Email - Reject Invoice : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                # start a future for a thread
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))
                        else:
                            response_service[index] = data

                        del request_data_collection[future]

        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))


def process_rework_invoice(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        """
        Rework akan dikirim ke Maker Partner untuk case confirmation = 1
        """
        if payment_method not in ["DF", "VF"]:
            return None

        fitur_id = EmailnotifConfig.REWORK_INVOICE
        trx_type = "Reject Invoice"
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Pending Transaction " + invoice_id
        template_dir = "resources/email_template/make_invoice_template.html"

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = invoice.detail_mcs[0]

        amount = fmt.int_to_money(amount=float(invoice.amount))
        approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id)
        
        users_anchor = validate.get_user_maker(
            entitas_id=invoice.entitas_anchor,
            payment_method=payment_method,
            mcs_type=invoice.mcs_type,
            group_name=invoice.group_name,
            amount_invoice=invoice.amount
        )

        # penentuan confirmation
        # need_confirmation = False if invoice.inbox == bs.InvDataOngoing.INBOX_INVAP else True if invoice.confirmation == bs.InvDataOngoing.WITH_CONFIRMATION else False
        # if need_confirmation:
        #     users_partner = validate.get_user_maker(
        #         entitas_id=invoice.entitas_anchor,
        #         payment_method=payment_method,
        #         mcs_type="BASIC",
        #         group_name=None,
        #         amount_invoice=invoice.amount
        #     )

        #     users_anchor = users_anchor + users_partner

        users = [user.detail_user.email for user in users_anchor]
        list_data_log = []
        for user in users:
            email_params = {
                "event_trx": trx_type,
                "invoice_id": invoice_id,
                "reference_id": invoice.entitas_inv_no,
                "amount": amount,
                "trx_type": trx_type,
                "payment_method": payment_method,
                "payment_date": invoice.payment_date.strftime("%d-%m-%Y"),
                "user_name": approval_ongoing[0].user_name,
                "submitted_at": approval_ongoing[0].approval_date.strftime("%d-%m-%Y %H:%M:%S")
            }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": user,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=user,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)

        # generate request ke bri-notification
        logging.info("Send Email - Reowrk Invoice : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                # start a future for a thread
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))
                        else:
                            response_service[index] = data

                        del request_data_collection[future]

        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

        return None

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))


def process_revision_invoice(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        """
        Revision di request dari partner
        """
        if payment_method not in ["DF", "VF"]:
            return None

        fitur_id = EmailnotifConfig.REVISION_INVOICE
        trx_type = "Revision Invoice"
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Pending Transaction " + invoice_id
        template_dir = 'resources/email_template/make_invoice_template.html'

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = invoice.detail_mcs[0]

        amount = fmt.int_to_money(amount=float(invoice.amount))
        approval_ongoing = db.get_approval_ongoing(invoice_id=invoice_id, user_id=user_id)

        users_anchor = validate.get_user_maker(
            entitas_id=invoice.entitas_anchor,
            payment_method=payment_method,
            mcs_type=invoice.mcs_type,
            group_name=invoice.group_name,
            amount_invoice=invoice.amount
        )

        users_partner = validate.get_user_maker(
            entitas_id=invoice.entitas_anchor,
            payment_method=payment_method,
            mcs_type="BASIC",
            group_name=None,
            amount_invoice=invoice.amount
        )

        users_anchor = users_anchor + users_partner

        users = [user.detail_user.email for user in users_anchor]
        for user in users:
            email_params = {
                "event_trx": trx_type,
                "invoice_id": invoice_id,
                "reference_id": invoice.entitas_inv_no,
                "amount": amount,
                "trx_type": trx_type,
                "payment_method": payment_method,
                "payment_date": invoice.payment_date.strftime("%d-%m-%Y"),
                "user_name": approval_ongoing[0].user_name,
                "submitted_at": approval_ongoing[0].approval_date.strftime("%d-%m-%Y %H:%M:%S")
            }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": user,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=user,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)

        
        # generate request ke bri-notification
        logging.info("Send Email - Revision Invoice : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                # start a future for a thread
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))
                        else:
                            response_service[index] = data

                        del request_data_collection[future]

        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

        return None

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))