import sys
import time
import json
import logging
import datetime

import traceback
import htmlmin
import secrets
import numpy as np
import queue
import concurrent.futures

import helper.base as bs
import helper.database as db
import helper.service as svc
import helper.validation as validate

import libs.formatter as fmt

from config.constant import EmailnotifConfig


# Email hanya untuk DF dan VF
def process_actual_settlement(invoice_id: str = None, user_id: str = None, payment_method: str = None):
    try:
        if payment_method not in ["DF", "VF"]:
            return None

        fitur_id = EmailnotifConfig.ACTUAL_SETTLEMENT
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Settlement " + invoice_id

        invoice = db.get_detail_invoice(invoice_id=invoice_id, payment_method=payment_method)
        invoice_mcs = None
        invoice_vf = None

        if payment_method == "DF":
            invoice_mcs = invoice.detail_mcs[0]

        else:
            invoice_vf = invoice.detail_vfdone[0]

        # terkait seller dan buyer name
        seller = invoice.entitas_partner_name if payment_method == "VF" else invoice.entitas_anchor
        buyer = invoice.entitas_anchor_name if payment_method == "VF" else invoice.entitas_partner_name

        amount = fmt.int_to_money(amount=float(invoice.amount))
        actual_disburse_date = invoice_vf.disbursement_date if payment_method == "VF" else invoice_mcs.disbursement_date
        actual_settle_date = invoice_vf.settlement_date if payment_method == "VF" else invoice_mcs.settlement_date
        disburse_date = invoice.payment_date.strftime("%d-%m-%Y")
        maturity_date = invoice.maturity_date.strftime("%d-%m-%Y")

        # untuk template
        if payment_method == "DF":
            template_dir = "resources/email_template/df/actual_settlement_template.html"

        else:
            template_dir = "resources/email_template/vf/actual_settlement_template.html"

        # get user
        users = []
        users_partner = db.get_email_entitas(entitas_id=invoice.entitas_anchor, partner_id=invoice.entitas_partner, payment_method=invoice.payment_method, get_from="partner")
        users_partner = users_partner[0].email_partner.split("|") if users_partner else []
        users = users + users_partner

        # checker parent
        entitas_parent_partner = db.get_detail_entitas(entitas_id=invoice.entitas_partner)
        approval_parent_partner = db.get_approval_ongoing(invoice_id=invoice_id, entitas_id=entitas_parent_partner.parent_id, is_valid=True)
        if approval_parent_partner:
            users_parent_partner = db.get_email_entitas(entitas_id=entitas_parent_partner.entitas_id, payment_method=payment_method, get_from="anchor")
            users_parent_partner = users_parent_partner[0].email_anchor.split("|") if users_parent_partner else []
            users = users + users_parent_partner

        # untuk email anchor - VF
        if payment_method == "VF":
            users_anchor = db.get_email_entitas(entitas_id=invoice.entitas_anchor, payment_method=invoice.payment_method, get_from="anchor")
            users_anchor = users_anchor[0].email_anchor.split("|") if users_anchor else []

            users = users + users_anchor

            # check parent
            entitas_parent_anchor = db.get_detail_entitas(entitas_id=invoice.entitas_anchor)
            approval_parent_anchor = db.get_approval_ongoing(invoice_id=invoice_id, entitas_id=entitas_parent_anchor.parent_id, is_valid=True)
            if approval_parent_anchor:
                users_parent_anchor = db.get_email_entitas(entitas_id=entitas_parent_anchor.entitas_id, payment_method=payment_method, get_from="anchor")
                users_parent_anchor = users_parent_anchor[0].email_anchor.split("|") if users_parent_anchor else []
                users = users + users_parent_anchor

        list_data_log = []
        for user in users:
            if payment_method == "DF":
                email_params = {
                    "email": user,
                    "invoice_id": invoice_id,
                    "payment_method": payment_method,
                    "seller": seller,
                    "buyer": buyer,
                    "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
                    "account_settlement": invoice.account_settlement,
                    "loan_account": invoice.account_disbursement,
                    "amount": amount,
                    "disbursement_date": actual_disburse_date.strftime("%d-%m-%Y"),
                    "maturity_date": maturity_date,
                    "actual_settle_date": actual_settle_date.strftime("%d-%m-%Y")
                }

            elif payment_method == "VF":
                prepaid_int = fmt.int_to_money(amount=float(float(invoice_vf.interest_anchor) + float(invoice_vf.interest_vendor)))
                int_vendor = fmt.int_to_money(amount=float(invoice_vf.interest_vendor))
                int_anchor = fmt.int_to_money(amount=float(invoice_vf.interest_anchor))
                disburse_amount_k2 = fmt.int_to_money(amount=float(float(invoice.amount) - (float(invoice_vf.interest_anchor) + float(invoice_vf.interest_vendor)) ))

                email_params = {
                    "email": user,
                    "invoice_id": invoice_id,
                    "payment_method": invoice.payment_method,
                    "seller": seller,
                    "buyer": buyer,
                    "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
                    "account_settlement": invoice.account_settlement,
                    "loan_vendor": invoice_vf.loan_vendor,
                    "amount": amount,
                    "disbursement_date": actual_disburse_date.strftime("%d-%m-%Y"),
                    "maturity_date": maturity_date,
                    "actual_settle_date": actual_settle_date.strftime("%d-%m-%Y")
                }

            message = fmt.generate_email(template_dir, email_params)
            min_message = htmlmin.minify(message)

            # generate email_id
            email_id = None
            id_email_exist = True
            while id_email_exist:
                email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
                log_email_exist = db.get_logged_email(id_email=email_id)
                id_email_exist = True if log_email_exist else False

            data_log = {
                "id_email": email_id,
                "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
                "fitur_id": fitur_id,
                "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
                "to_email": user,
                "subject": subject,
                "content": min_message,
                "respon_status": 6,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }

            # insert log
            db.insert_log_email(
                id_email=email_id,
                send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
                fitur=fitur_id,
                from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
                to_email=user,
                subject=subject,
                content=min_message,
                respon_status=6,
                respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
            )

            list_data_log.append(data_log)


        # generate request ke bri-notification
        logging.info("Send Email - Invoice Payment : " + str(invoice_id))
        response_service = {}
        section = int(len(list_data_log) / 4) + 1
        split_data_log = np.array_split(list_data_log, section)
        for sp_data_log in split_data_log:
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                # start a future for a thread
                request_data_collection = {}
                for index, data in enumerate(sp_data_log):
                    request_data_collection[executor.submit(
                        svc.request_send_email,
                        recipient=data["to_email"],
                        subject=data["subject"],
                        message=data["content"]
                    )] = data["id_email"]

                while request_data_collection:
                    done, not_done = concurrent.futures.wait(
                        request_data_collection, timeout=5,
                        return_when=concurrent.futures.FIRST_COMPLETED
                    )

                    for future in done:
                        index = request_data_collection[future]

                        try:
                            data = future.result()
                        except Exception as exc:
                            logging.warning("%i generated an exception: %s", (index, exc))
                        else:
                            response_service[index] = data

                        del request_data_collection[future]


        # update data log
        for index, data in enumerate(list_data_log):
            result_send_email = response_service[data["id_email"]]
            db.update_log_email(
                id_email=data["id_email"],
                to_respon_status=result_send_email["respon_status"],
                to_respon_date=result_send_email["respon_date"]
            )

        return None

    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))

    