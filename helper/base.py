import time
import datetime

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Date, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.dialects.mysql import DOUBLE, LONGTEXT

from config.constant import EmailnotifConfig

connString = {
    "hostname": EmailnotifConfig.IPDB,
    "port": EmailnotifConfig.PORTDB,
    "user": EmailnotifConfig.USERDB,
    "key": EmailnotifConfig.PASSDB,
    "dbname": EmailnotifConfig.NAMEDB
}

Base = declarative_base()
DB_URL = "mysql+pymysql://{user}:{passwd}@{host}:{port}/{dbname}".format(
    user=connString["user"],
    passwd=connString["key"],
    host=connString["hostname"],
    port=connString["port"],
    dbname=connString["dbname"]
)

engine = create_engine(DB_URL, pool_recycle=15, pool_pre_ping=True)
Session = sessionmaker(bind=engine)

class Settings(Base):
    __tablename__ = "setting"

    parameter = Column(String(255), primary_key=True, nullable=False)

    values = Column(String(255), default="")


class User(Base):
    __tablename__ ="user"

    user_id = Column(String(8), primary_key=True, nullable=False)

    personal_number = Column(Integer, default=None)

    username = Column(String(60), default=None)

    password = Column(String(120), default=None)

    name = Column(String(100), default=None)

    email = Column(String(60), default=None)

    entitas_id  = Column(String(5), default=None)

    status = Column(Integer, default=None)

    detail_kewenangan = relationship("MstMapKewenangan", backref="detail_user", primaryjoin="User.user_id==MstMapKewenangan.user_id")


class InvDataOngoing(Base):
    __tablename__ = "inv_data_ongoing"

    INBOX_INVAP = 1
    INBOX_INVAR = 0

    WITH_CONFIRMATION = 1
    WITHOUT_CONFIRMATION = 0

    invoice_id = Column(String(19), primary_key=True)

    invoice_type = Column(String(10), default=None)

    entitas_anchor = Column(String(5), default=None)

    entitas_anchor_name = Column(String(200), default=None)

    entitas_partner = Column(String(5), default=None)

    entitas_partner_name = Column(String(200), default=None)

    payment_method = Column(String(3), default=None)

    payment_method_name = Column(String(100), default=None)

    payment_method_confirm = Column(String(3), default=None)

    payment_method_confirm_name = Column(String(100), default=None)

    currency = Column(String(3), default=None)

    amount = Column(DOUBLE, default=0)

    account_receivable = Column(String(18), default=None)

    account_receivable_confirm = Column(String(18), default=None)

    account_disbursement = Column(String(18), default=None)

    account_disbursement_confirm = Column(String(18), default=None)

    account_settlement = Column(String(18), default=None)

    account_settlement_confirm = Column(String(18), default=None)

    account_anchor = Column(String(18), default=None)

    account_escrow = Column(String(18), default=None)

    settlement_type = Column(String(18), default=None)

    create_date = Column(DateTime)

    approve_date = Column(DateTime)

    payment_date = Column(DateTime)

    maturity_date = Column(DateTime)

    sharing_date = Column(DateTime)

    interest_sharing_type = Column(String(5), default=None)

    entitas_inv_no = Column(String(200), default=None)

    notes = Column(String(200), default=None)

    remark = Column(String(200), default=None)

    attachment = Column(LONGTEXT, default=None)

    confirmation = Column(Integer, default=None)

    simulation_data = Column(LONGTEXT, default=None)

    simulation_status = Column(Integer, default=None)

    inbox = Column(Integer, default=None)

    payment_date_ar = Column(DateTime)

    relation_id = Column(String(15), default=None)

    account_receivable_ark = Column(String(18), default=None)

    account_receivable_name = Column(String(100), default=None)

    document_name = Column(LONGTEXT, default=None)

    document_path = Column(LONGTEXT, default=None)

    mcs_type = Column(String(100), default=None)

    group_name = Column(String(100), default=None)

    partner_mcs_type = Column(String(100), default=None)

    partner_group_name = Column(String(100), default=None)

    detail_approval_ongoing = relationship("InvApprovalOngoing", backref="inv_ongoing_approval", primaryjoin="InvApprovalOngoing.invoice_id==InvDataOngoing.invoice_id")

    detail_vf_done = relationship("InvDataVFDone", backref="detail_ongoing_vfdone", primaryjoin="InvDataOngoing.invoice_id==InvDataVFDone.invoice_id")

    detail_vf = relationship("InvDataVF", backref="detail_ongoing_vf", primaryjoin="InvDataOngoing.invoice_id==InvDataVF.invoice_id")

    detail_mcs = relationship("InvDataMcs", backref="detail_ongoing_mcs", primaryjoin="InvDataMcs.invoice_id==InvDataOngoing.invoice_id")


class InvDataMcs(Base):
    __tablename__ = "inv_data_mcs"

    STATUS_INITIAL = 0
    STATUS_PENDING_APPROVE_CHECKER = 1
    STATUS_PENDING_APPROVE_SIGNER = 2
    STATUS_APPROVED = 3
    STATUS_REJECTED = 4
    STATUS_REWORK = 5
    STATUS_MAKER_FROM_REWORK = 505
    STATUS_CONFIRMATION_PENDING_CHECKER = 6
    STATUS_CONFIRMATION_PENDING_SIGNER = 7
    STATUS_CONFIRMATION_APPROVED = 8
    STATUS_CONFIRMATION_REJECTED = 9
    STATUS_INVOICE_EXPIRED = 11
    STATUS_INVOICE_CONFIRMATION_EXPIRED = 12
    STATUS_CANCEL_PENDING_CHECKER = 13
    STATUS_CANCEL_PENDING_SIGNER = 14
    STATUS_CANCEL_APPROVE = 15
    STATUS_CANCEL_REJECT = 16

    STATUS_PENDING_MAKER_PARENT = 31
    STATUS_PENDING_CHECKER_PARENT = 32
    STATUS_PENDING_SIGNER_PARENT = 33

    STATUS_PENDING_MAKER_PARENT_CONFIRM = 34
    STATUS_PENDING_CHECKER_PARENT_CONFIRM = 35
    STATUS_PENDING_SIGNER_PARENT_CONFIRM = 36

    DISBURSEMENT_STATUS_NOT_DISBURSE = 0
    DISBURSEMENT_STATUS_SUCCESS = 1
    DISBURSEMENT_STATUS_RETRY = 3
    DISBURSEMENT_STATUS_FAILED = 4

    SETTLEMENT_STATUS_NOT_DISBURSE = 0
    SETTLEMENT_STATUS_SUCCESS = 1
    SETTLEMENT_STATUS_RETRY = 3
    SETTLEMENT_STATUS_FAILED = 4

    HOLD_STATUS_NOT_HOLD = 0
    HOLD_STATUS_SUCCESS = 1
    HOLD_STATUS_RETRY = 3
    HOLD_STATUS_FAILED = 4

    ADVPAY_STATUS_NOT_ADVPAY = 0
    ADVPAY_STATUS_PENDING_CHECKER = 17
    ADVPAY_STATUS_PENDING_SIGNER = 18
    ADVPAY_STATUS_APPROVE = 19
    ADVPAY_STATUS_REJECT = 20

    ADVPAY_STATUS_PENDING_MAKER_PARENT = 40
    ADVPAY_STATUS_PENDING_CHECKER_PARENT = 32
    ADVPAY_STATUS_PENDING_SIGNER_PARENT = 33

    ADVSET_STATUS_NOT_ADVSET = 0
    ADVSET_STATUS_PENDING_CHECKER = 21
    ADVSET_STATUS_PENDING_SIGNER = 22
    ADVSET_STATUS_APPROVE_FACPAY = 23
    ADVSET_STATUS_REJECT_FACPAY = 24

    ADVSET_STATUS_PENDING_MAKER_PARENT = 43
    ADVSET_STATUS_PENDING_CHECKER_PARENT = 32
    ADVSET_STATUS_PENDING_SIGNER_PARENT = 33

    INSTAPAY_STATUS_NOT_INSTAPAY = 0
    INSTAPAY_STATUS_PENDING_CHECKER = 26
    INSTAPAY_STATUS_PENDING_SIGNER = 27
    INSTAPAY_STATUS_APPROVE = 28
    INSTAPAY_STATUS_REJECT = 29

    INSTAPAY_STATUS_PENDING_MAKER_PARENT = 46
    INSTAPAY_STATUS_PENDING_CHECKER_PARENT = 32
    INSTAPAY_STATUS_PENDING_SIGNER_PARENT = 33

    CANCEL_STATUS_NOT_CANCEL = 0
    CANCEL_STATUS_PENDING_CHECKER = 13
    CANCEL_STATUS_PENDING_SIGNER = 14
    CANCEL_STATUS_APPROVE = 15
    CANCEL_STATUS_REJECT = 16

    CANCEL_STATUS_PENDING_MAKER_PARENT = 37
    CANCEL_STATUS_PENDING_CHECKER_PARENT = 32
    CANCEL_STATUS_PENDING_SIGNER_PARENT = 33

    id_mcs = Column(Integer, primary_key=True)

    invoice_id = Column(String(19), ForeignKey("inv_data_ongoing.invoice_id"), primary_key=True)

    maker = Column(Integer, default=None)

    checker = Column(Integer, default=None)

    signer = Column(Integer, default=None)

    status = Column(Integer, default=None)

    maker_confirm = Column(Integer, default=None)

    checker_confirm = Column(Integer, default=None)

    signer_confirm = Column(Integer, default=None)

    status_confirm = Column(Integer, default=None)

    maker_cancel = Column(Integer, default=None)

    checker_cancel = Column(Integer, default=None)

    signer_cancel = Column(Integer, default=None)

    status_cancel = Column(Integer, default=None)

    maker_advpay = Column(Integer, default=None)

    checker_advpay = Column(Integer, default=None)

    signer_advpay = Column(Integer, default=None)

    status_advpay = Column(Integer, default=None)

    maker_advset = Column(Integer, default=None)

    checker_advset = Column(Integer, default=None)

    signer_advset = Column(Integer, default=None)

    status_advset = Column(Integer, default=None)

    checker_instapay = Column(Integer, default=None)

    signer_instapay = Column(Integer, default=None)

    status_instapay = Column(Integer, default=None)

    status_dispute = Column(Integer, default=None)

    disbursement_status = Column(Integer, default=None)

    disbursement_date = Column(DateTime, default=None)

    settlement_status = Column(Integer, default=None)

    settlement_date = Column(DateTime, default=None)

    paid_status = Column(Integer, default=None)

    paid_date = Column(DateTime)

    hold_status = Column(Integer, default=None)

    hold_date = Column(DateTime, default=None)

    hold_reference = Column(String(50), default=None)

    unhold_date = Column(DateTime, default=None)

    entitas_advset_arp = Column(String(11), default=None)

    last_order = Column(Integer, default=0)


class InvDataVF(Base):
    __tablename__ = "inv_data_vf"

    id_vf = Column(Integer, nullable=False, autoincrement=True, primary_key=True)

    app_name = Column(String(10), default=None)

    invoice_id = Column(String(19), ForeignKey("inv_data_ongoing.invoice_id"), default=None)

    invoice_type = Column(String(10), default=None)

    entitas_anchor = Column(String(5), default=None)

    entitas_anchor_name = Column(String(200), default=None)

    entitas_partner = Column(String(5), default=None)

    entitas_partner_name = Column(String(200), default=None)

    payment_method = Column(String(2), default=None)

    payment_method_name = Column(String(100), default=None)

    currency = Column(String(3), default=None)

    disbursement_date_input = Column(Date, default=None)

    loan_vendor = Column(String(15), default=None)

    ops_anchor = Column(String(15), default=None)

    esc_vendor = Column(String(15), default=None)

    ops_vendor = Column(String(15), default=None)

    notes = Column(String(200), default=None)

    remark = Column(String(40), default=None)

    hold_reference = Column(String(50), default=None)

    hold_status = Column(Integer, default=None)

    hold_date = Column(DateTime, default=None)

    unhold_date = Column(DateTime, default=None)

    disbursement_status = Column(Integer, default=None)

    disbursement_date = Column(DateTime, default=None)

    disbursement_retry = Column(Integer, default=None)

    disbursement_amount_K2 = Column(DOUBLE, default=0)

    settlement_status = Column(Integer, default=None)

    settlement_date = Column(DateTime, default=None)

    settlement_retry = Column(Integer, default=None)

    status_dispute = Column(Integer, default=None)

    disbursement_account_D = Column(String(15), default=None)

    settlement_account_D = Column(String(15), default=None)

    overdue_status = Column(Integer, default=None)

    disbursement_report = Column(Integer, default=None)

    settlement_report = Column(Integer, default=None)


class InvDataVFDone(Base):
    __tablename__ = "inv_data_vf_done"

    id_vf = Column(Integer, autoincrement=True, primary_key=True, nullable=False)

    app_name = Column(String(10), default=None)

    invoice_id = Column(String(19), ForeignKey("inv_data_ongoing.invoice_id"), default=None)

    invoice_type = Column(String(19), default=None)

    entitas_anchor = Column(String(5), default=None)

    entitas_anchor_name = Column(String(200), default=None)

    entitas_partner = Column(String(5), default=None)

    entitas_partner_name = Column(String(200), default=None)

    payment_method = Column(String(2), default=None)

    payment_method_name = Column(String(100), default=None)

    disbursement_date_input = Column(Date, default=None)

    settlement_date_input = Column(Date, default=None)

    disbursement_status = Column(Integer, default=0)

    disbursement_date = Column(DateTime, default=None)

    settlement_status = Column(Integer, default=0)

    settlement_date = Column(DateTime, default=None)

    settlement_report = Column(Integer, default=None)

    # detail_ongoing = relationship("InvDataOngoing", backref="detail_vfdone_ongoing", primaryjoin="InvDataOngoing.invoice_id==InvDataVFDone.invoice_id")


class Entitas(Base):
    __tablename__ = "entitas"

    ENTITAS_TYPE_CV = "CV"
    ENTITAS_TYPE_PT = "PT"

    TCM_STATUS_TRUE = 1
    TCM_STATUS_FALSE = 0

    entitas_id = Column(String(15), primary_key=True, nullable=False)

    entitas_name = Column(String(100), default=None)

    entitas_level = Column(Integer, default=3)

    entitas_form = Column(String(10), default=None)

    entitas_type = Column(String(10), default=None)

    akta_pendirian = Column(String(100), default=None)

    akta_pendirian_date = Column(Date, default=None)

    akta_perubahan = Column(String(100), default=None)

    akta_perubahan_date = Column(Date, default=None)

    entitas_npwp = Column(String(20), default=None)

    entitas_phone = Column(String(20), default=None)

    entitas_address = Column(String(200), default=None)

    deletable = Column(Integer, default=1)

    status = Column(Integer, default=1)

    registrasi_id = Column(Integer, default=None)

    token_api = Column(String(255), default=None)

    note = Column(String(255), default=None)

    parent_status = Column(Integer, default=None)

    parent_id = Column(String(5), default=None)

    tcm_status = Column(Integer, default=None)


class EmailLog(Base):
    __tablename__ = "email_log"

    id_email = Column(String(255), primary_key=True, nullable=False, default="")

    send_date = Column(DateTime, default=None)

    fitur = Column(String(100), default=None)

    from_email = Column(String(255), default=None)

    to_email = Column(String(255), default=None)

    subject = Column(String(255), default=None)

    content = Column(LONGTEXT, default=None)

    respon_status = Column(Integer, default=None)

    respon_date = Column(DateTime, default=None)

    def __init__(self, id_email, send_date, fitur, from_email, to_email, subject, content, respon_status, respon_date=None):
        self.id_email = id_email
        self.send_date = send_date
        self.fitur = fitur
        self.from_email = from_email
        self.to_email = to_email
        self.subject = subject
        self.content = content
        self.respon_status = respon_status

        if respon_date:
            self.respon_date = respon_date

        else:
            self.respon_date = datetime.datetime.now()


class MstMapPartner(Base):
    __tablename__ = "mst_map_partner"

    REQUIRED_DOCUMENT_UPLOAD = 1
    NOTREQUIRED_DOCUMENT_UPLOAD = 0

    anchor_id = Column(String(5), primary_key=True, nullable=False)

    partner_id = Column(String(5), primary_key=True, nullable=False)

    fitur_id = Column(String(5), primary_key=True, nullable=False)

    fitur_name = Column(String(100), default=None)

    receive_account = Column(String(18), default=None)

    receive_name = Column(String(255), default=None)

    escrow_account = Column(String(18), default=None)

    escrow_name = Column(String(255), default=None)

    disbursement_account = Column(String(18), default=None)

    disbursement_name = Column(String(255), default=None)

    settlement_type = Column(String(1), default=None)

    settlement_account = Column(String(18), default=None)

    settlement_name = Column(String(255), default=None)

    anchor_account = Column(String(18), default=None)

    anchor_account_name = Column(String(255), default=None)

    interest_sharing_type = Column(String(5), default=None)

    partner_upload = Column(Integer, primary_key=True, nullable=False)

    maturity_days = Column(Integer, default=None)

    grace_period = Column(Integer, default=0)

    document_upload = Column(Integer, default=0)

    email_partner = Column(LONGTEXT, default=None)


class MstMapAnchor(Base):
    __tablename__ = "mst_map_anchor"

    entitas_anchor = Column(String(5), primary_key=True, nullable=False)

    fitur_id = Column(String(3), primary_key=True, nullable=False)

    fitur_name = Column(String(100), default=None)

    email_anchor = Column(LONGTEXT, default=None)


class InvApprovalOngoing(Base):
    __tablename__ = "inv_approval_ongoing"

    CRITERIA_APPROVE = "approve"
    CRITERIA_CONFIRM = "confirm"
    CRITERIA_APPROVE_PARENT = "approve parent"
    CRITERIA_CONFIRM_PARENT = "confirm parent"

    CRITERIA_CANCEL = "cancel"
    CRITERIA_CANCEL_PARENT = "cancel parent"

    CRITERIA_FACPAY = "advset"
    CRITERIA_FACPAY_PARENT = "advset parent"

    CRITERIA_INSTAPAY = "instapay"
    CRITERIA_INSTAPAY_PARENT = "instapay parent"

    CRITERIA_ADVPAY = "advpay"
    CRITERIA_ADVPAY_PARENT = "advpay parent"
    
    appoval_id = Column(Integer, nullable=False, autoincrement=True, primary_key=True)

    entitas_id = Column(String(5), default=None)

    invoice_id = Column(String(19), ForeignKey("inv_data_ongoing.invoice_id"), default=None)

    user_id = Column(String(15), ForeignKey("user.user_id"), default=None)

    user_name = Column(String(100), default=None)

    user_privileges = Column(String(100), default=None)

    status = Column(Integer, default=None)

    note = Column(String(40000), default=None)

    approval_date = Column(DateTime, default=None)

    criteria = Column(String(100), default=None)

    flag = Column(Integer, default=None)

    batch_id = Column(String(50),  default=None)

    document_upload = Column(LONGTEXT, default=None)

    user_email = Column(String(200), default=None)

    def __init__(self, entitas_id, invoice_id, user_id, user_name, user_privileges, status, note, criteria, flag, batch_id = None, document_upload = None, approval_date = None):
        self.entitas_id = entitas_id
        self.invoice_id = invoice_id
        self.user_id = user_id
        self.user_name = user_name
        self.user_privileges = user_privileges
        self.status = status
        self.note = note
        self.criteria = criteria
        self.flag = flag

        if batch_id:
            self.batch_id = batch_id

        if document_upload:
            self.document_upload = document_upload

        if approval_date:
            self.approval_date = approval_date


class MstMapKewenangan(Base):
    __tablename__ = "mst_map_kewenangan"

    MCS_TYPE_GROUP = "GROUP"
    MCS_TYPE_BASIC = "BASIC"

    ORDER_STATUS_TRUE = "TRUE"
    ORDER_STATUS_FALSE = "FALSE"

    user_id = Column(String(8), ForeignKey("user.user_id"), primary_key=True, nullable=False)

    entitas_id = Column(String(5), primary_key=True, nullable=False)

    fitur_id = Column(String(3), primary_key=True, nullable=False)

    fitur_name = Column(String(50), default=None)

    role_id = Column(Integer, primary_key=True, nullable=False)

    limit_min = Column(Integer, default=None)

    limit_max = Column(Integer, default=None)

    action_order = Column(Integer, default=None)

    tcm_status = Column(Integer, default=None)

    entitas_child = Column(String(5), default=None)

    mcs_type = Column(String(15), default="BASIC")

    group_name = Column(String(50), default=None)

    group_name_alias = Column(String(50), default=None)

    currency = Column(String(50), default=None)

    order_status = Column(String(15), default='FALSE')

    detail_role = relationship("Role", backref="detail_kewenangan", primaryjoin="Role.role_id==MstMapKewenangan.role_id")
    
    def __init__(self, user_id, entitas_id, fitur_id, fitur_name, role_id, limit_min, limit_max):
        self.user_id = user_id
        self.entitas_id = entitas_id
        self.fitur_id = fitur_id
        self.fitur_name = fitur_name
        self.role_id = role_id
        self.limit_min = limit_min
        self.limit_max = limit_max


class Role(Base):
    __tablename__ = "role"

    role_id = Column(Integer, ForeignKey("mst_map_kewenangan.role_id"), primary_key=True, nullable=False)

    role_name = Column(String(50), default=None)

    deletable = Column(Integer, default=1)

    level_entitas = Column(Integer, default=None)

