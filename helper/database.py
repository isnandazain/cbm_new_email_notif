import time
import logging
import traceback

import json
import datetime
import sqlalchemy
from sqlalchemy import cast, Date, and_, or_, func, text, case

import helper.base as bs

session = bs.Session()
logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s")

def rollback_connection():
    session.rollback()

def expired_session():
    session.expire_all()

def close_session():
    session.close()

def get_reminder_invoices(payment_method: str, h_minus: int):
    filter_date = datetime.date.today() + datetime.timedelta(days=h_minus)

    if payment_method == "DF":
        filters = [
            bs.InvDataOngoing.payment_method == "DF",
            bs.InvDataMcs.disbursement_status == 1,
            bs.InvDataMcs.settlement_status != 1,
            bs.InvDataOngoing.maturity_date == filter_date
        ]

        invoices = session.query(bs.InvDataOngoing).filter(
            *filters
        ).join(
            bs.InvDataMcs, bs.InvDataMcs.invoice_id == bs.InvDataOngoing.invoice_id
        ).all()

    else: # untuk VF
        filters = [
            bs.InvDataOngoing.payment_method == "VF",
            bs.InvDataVF.disbursement_status == 1,
            bs.InvDataVF.settlement_status != 1,
            bs.InvDataOngoing.maturity_date == filter_date
        ]

        invoices = session.query(bs.InvDataOngoing).filter(
            *filters
        ).join(
            bs.InvDataVF, bs.InvDataVF.invoice_id == bs.InvDataOngoing.invoice_id
        ).all()
    
    return invoices


def get_stop_invoices(payment_method: str, interval: int):
    date_ago = datetime.date.today() - datetime.timedelta(days=1)

    if payment_method == "DF":
        filters = [
            bs.InvDataMcs.disbursement_status == 1,
            bs.InvDataMcs.settlement_status != 1,
            bs.InvDataOngoing.payment_method == "DF",
            cast(func.now(), Date) == text("date(inv_data_ongoing.maturity_date + interval mst_map_partner.grace_period + {d_interval} day)".format(d_interval=interval))
        ]

        invoices = session.query(bs.InvDataOngoing).filter(
            *filters
        ).join(
            bs.MstMapPartner, and_(
                bs.InvDataOngoing.payment_method == bs.MstMapPartner.fitur_id,
                bs.InvDataOngoing.entitas_anchor == bs.MstMapPartner.anchor_id,
                bs.InvDataOngoing.entitas_partner == bs.MstMapPartner.partner_id
            )
        ).join(
            bs.InvDataMcs, bs.InvDataMcs.invoice_id==bs.InvDataOngoing.invoice_id
        ).all()

    else:
        filters = [
            bs.InvDataMcs.disbursement_status == 1,
            bs.InvDataMcs.settlement_status != 1,
            bs.InvDataOngoing.payment_method == "VF",
            cast(func.now(), Date) == text("date(inv_data_ongoing.maturity_date + interval mst_map_partner.grace_period + {d_interval} day)".format(d_interval=interval))
        ]

        invoices = session.query(bs.InvDataOngoing).filter(
            *filters
        ).join(
            bs.InvDataVF, bs.InvDataOngoing.invoice_id == bs.InvDataVF.invoice_id
        ).join(
            bs.MstMapPartner, case(
                [
                    (and_(
                        bs.InvDataOngoing.inbox == 0, 
                        bs.InvDataOngoing.payment_method == bs.MstMapPartner.fitur_id
                    ), and_(bs.InvDataOngoing.entitas_anchor == bs.MstMapPartner.partner_id, bs.InvDataOngoing.entitas_partner == bs.MstMapPartner.anchor_id)),
                    (and_(
                        bs.InvDataOngoing.inbox == 1, 
                        bs.InvDataOngoing.payment_method == bs.MstMapPartner.fitur_id
                    ), and_(bs.InvDataOngoing.entitas_anchor == bs.MstMapPartner.anchor_id, bs.InvDataOngoing.entitas_partner == bs.MstMapPartner.partner_id))
                ]
            )
        ).all()

    return invoices


def get_detail_invoice(invoice_id: str = None, payment_method: str = None):
    filters = []
    if payment_method == "DF":
        if invoice_id != None:
            filters.append(bs.InvDataOngoing.invoice_id == invoice_id)

            invoice = session.query(bs.InvDataOngoing).filter(
                *filters
            ).join(
                bs.InvDataMcs, bs.InvDataMcs.invoice_id == bs.InvDataOngoing.invoice_id
            ).first()

    else:
        if invoice_id != None:
            filters.append(bs.InvDataOngoing.invoice_id == invoice_id)

        invoice = session.query(bs.InvDataOngoing).filter(
            *filters
        ).join(
            bs.InvDataVF, bs.InvDataOngoing.invoice_id == bs.InvDataVF.invoice_id
        ).first()

    return invoice


def get_approval_ongoing(invoice_id: str = None, user_id: str = None, entitas_id: str = None, criteria : str = None, status: int = None, multiple_status: list = None, 
                        multiple_criteria: list = None, is_valid: bool = False, group_by_query: str = None):
    filters = []
    
    if invoice_id != None:
        filters.append(bs.InvApprovalOngoing.invoice_id == invoice_id)

    if entitas_id != None:
        filters.append(bs.InvApprovalOngoing.entitas_id == entitas_id)

    if user_id != None:
        filters.append(bs.InvApprovalOngoing.user_id == user_id)

    if criteria != None:
        filters.append(bs.InvApprovalOngoing.criteria == criteria)

    if status != None:
        filters.append(bs.InvApprovalOngoing.status == status)

    if multiple_status != None:
        filters.append(bs.InvApprovalOngoing.status.in_(
            multiple_status
        ))

    if multiple_criteria != None:
        filters.append(bs.InvApprovalOngoing.criteria.in_(
            multiple_criteria
        ))

    group_apply = []
    if group_by_query:
        dict_groupby = {
            "user_id": bs.InvApprovalOngoing.user_id,
            "criteria": bs.InvApprovalOngoing.criteria,
            "status": bs.InvApprovalOngoing.status,
        }

        group_apply.append(dict_groupby[group_by_query])

    if is_valid:
        filters.append(bs.InvApprovalOngoing.flag == 1)

    approval_ongoing = session.query(bs.InvApprovalOngoing).filter(
        *filters
    ).order_by(
        bs.InvApprovalOngoing.approval_date.desc()
    ).group_by(
        *group_apply
    ).all()

    return approval_ongoing


def get_logged_email(id_email: str = None, multi_type_email: list = None, type_email: str = None, invoice_id: str = None, respon_status : int = None):
    filters = []

    if invoice_id != None:
        invoice_id = "%{}%".format(invoice_id)
        filters.append(bs.EmailLog.content.like(invoice_id))

    if type_email != None:
        filters.append(bs.EmailLog.fitur == type_email)

    if multi_type_email != None:
        filters.append(bs.EmailLog.fitur.in_(
            multi_type_email
        ))

    if respon_status != None:
        filters.append(and_(
            bs.EmailLog.respon_status >= 1,
            bs.EmailLog.respon_status <= respon_status
        ))

    if id_email != None:
        filters.append(bs.EmailLog.id_email == id_email)

    log_email = session.query(bs.EmailLog).filter(
        *filters
    ).all()

    return log_email


def insert_log_email(id_email = None, send_date = None, fitur = None, from_email = None, to_email = None, 
                     subject = None, content = None, respon_status = None, respon_date = None):
    ent_email_log = bs.EmailLog(
        id_email=id_email,
        send_date=send_date,
        fitur=fitur,
        from_email=from_email,
        to_email=to_email,
        subject=subject,
        content=content,
        respon_status=respon_status,
        respon_date=respon_date
    )

    session.add(ent_email_log)
    session.flush()
    session.commit()

    return True


def update_log_email(id_email = None, to_respon_status = None, to_respon_date = None):
    filters = []

    if id_email != None:
        filters.append(bs.EmailLog.id_email == id_email)

    log_email = session.query(bs.EmailLog).filter(
        *filters
    ).first()

    if to_respon_status != None:
        log_email.respon_status = to_respon_status

    if to_respon_date != None:
        log_email.respon_date = to_respon_date

    session.add(log_email)
    session.flush()
    session.commit()

    return True


def get_email_entitas(entitas_id: str = None, partner_id: str = None, payment_method: str = None, get_from: str = "partner"):
    if get_from == "partner":
        filters = [
            bs.MstMapPartner.anchor_id == entitas_id,
            bs.MstMapPartner.partner_id == partner_id,
            bs.MstMapPartner.fitur_id == payment_method
        ]

        users = session.query(bs.MstMapPartner).filter(
            *filters
        ).all()

    else:
        filters = [
            bs.MstMapAnchor.entitas_anchor == entitas_id,
            bs.MstMapAnchor.fitur_id == payment_method
        ]

        users = session.query(bs.MstMapAnchor).filter(
            *filters
        ).all()
    
    return users


def get_detail_entitas(entitas_id: str = None, parent_id: str = None):
    filters = []

    if entitas_id != None:
        filters.append(
            bs.Entitas.entitas_id == entitas_id
        )

    if parent_id != None:
        filters.append(
            bs.Entitas.parent_id == parent_id
        )

    entitas = session.query(bs.Entitas).filter(
        *filters
    ).first()

    return entitas


def get_mst_map_kewenangan(role_id: str = None, role_ids: list = None, mcs_type: str = None, group_name_alias: str = None, group_by_query: str = None, fitur_id: str = None,
                            action_order: int = None, entitas_id: str = None, user_id_out: str = None, amount_invoice: int = None):
    filters = []

    if role_id != None:
        filters.append(bs.MstMapKewenangan.role_id == role_id)

    if role_ids != None:
        filters.append(bs.MstMapKewenangan.role_id.in_(
            role_ids
        ))

    if amount_invoice:
        filters.append(and_(
            bs.MstMapKewenangan.limit_min <= amount_invoice,
            bs.MstMapKewenangan.limit_max >= amount_invoice
        ))

    if user_id_out != None:
        filters.append(bs.MstMapKewenangan.user_id != user_id_out)

    if mcs_type != None:
        filters.append(bs.MstMapKewenangan.mcs_type == mcs_type)

    if fitur_id != None:
        filters.append(bs.MstMapKewenangan.fitur_id == fitur_id)

    if entitas_id != None:
        filters.append(bs.MstMapKewenangan.entitas_id == entitas_id)

    if action_order != None:
        filters.append(bs.MstMapKewenangan.action_order == action_order)

    if group_name_alias != None:
        filters.append(bs.MstMapKewenangan.group_name_alias == group_name_alias)

    group_apply = []
    if group_by_query:
        dict_group = {
            "user_id": bs.MstMapKewenangan.user_id,
            "role_id": bs.MstMapKewenangan.user_id,
            "fitur_id": bs.MstMapKewenangan.fitur_id
        }

        group_apply.append(dict_group[group_by_query])


    kewenangan = session.query(bs.MstMapKewenangan).filter(
        *filters
    ).group_by(
        *group_apply
    ).all()

    return kewenangan

