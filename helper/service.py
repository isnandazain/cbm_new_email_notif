import time
import json
import logging
import datetime

import requests
import traceback

from requests import ConnectionError, ConnectTimeout, Timeout

from config.constant import EmailnotifConfig

logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s")


def request_send_email(recipient, subject, message):
    try:
        url = EmailnotifConfig.SERV_EMAIL_URL
        headers = {
            "Content-Type": "application/json",
            "Postman-Token": "4b998144-4765-4a9c-adbe-3cc5bc0763ef",
            "cache-control": 'no-cache'
        }

        request_data = {
            "email": EmailnotifConfig.SERV_EMAIL_SENDER,
            "password": EmailnotifConfig.SERV_EMAIL_PASSWORD,
            "email_name": EmailnotifConfig.SERV_EMAIL_NAME,
            "to_address": recipient,
            "cc_address": "",
            "subject": subject,
            "message": message
        }

        response = requests.post(url, data=json.dumps(request_data), headers=headers, timeout=5)
        if response.status_code == 200:
            response = response.json()
            if response["responseCode"] == "00":
                return {
                    "respon_status": 0,
                    "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
                }

            else:
                return {
                    "respon_status": 1,
                    "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
                }

        else:
            logging.warning("Stop invoice email failed")
            return {
                "respon_status": 1,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }
            
    except Exception as e:
        logging.warning(traceback.format_exc())
        return {
            "respon_status": 1,
            "respon_date": time.strftime("%Y-%m-%d %H:%M:%s")
        }


    