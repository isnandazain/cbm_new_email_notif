import json
import datetime
import traceback

import logging

import helper.base as bs
import helper.database as db

from config.constant import EmailnotifConfig


def validate_approve_invoicepayment(user_id, invoice, approval_ongoing):
    try:
        """
        STATUS INVOICE PAYMENT :
        - PENDING CHECKER = 17
        - PENDING SIGNER = 18
        - APPROVE = 19
        - REJECT =  20

        - PENDING CHECKER PARENT = 32
        - PENDING SIGNER PARENT = 33
        """

        invoice_mcs = invoice.detail_mcs[0]
        last_order = invoice.last_order + 1

        if invoice_mcs.status_advpay == 17:
            checkers = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checkers

        elif invoice_mcs.status_advpay == 18:
            signers = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signers

        elif invoice_mcs.status_advpay == 32:
            if last_order == 0: # tentukan user parent
                entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            checker_parents = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checker_parents

        elif invoice_mcs.status_advpay == 33:
            # pengecekan approval checker parent
            if last_order == 0:
                approval_to_checker = db.get_approval_ongoing(
                    invoice_id=invoice.invoice_id,
                    status=32,
                    criteria="advpay",
                    is_valid=True
                )

                if approval_to_checker:
                    entitas_id = user_id[:5]

                else:
                    entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            signer_parents = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signer_parents
        
        return []

    except Exception as e:
        logging.warning(traceback.format_exc())
        return []


def validate_approve_instapay(user_id, invoice, approval_ongoing):
    try:
        """
        STATUS INSTAPAY :
        - PENDING CHECKER = 26
        - PENDING SIGNER = 27
        - APPROVE = 28
        - REJECT = 29

        - PENDING CHECKER PARENT = 32
        - PENDING SIGNER PARENT = 33
        """
        
        invoice_mcs = invoice.detail_mcs[0]
        last_order = invoice_mcs.last_order + 1
        if invoice_mcs.status_instapay == bs.InvDataMcs.INSTAPAY_STATUS_PENDING_CHECKER:
            checkers = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checkers
        
        elif invoice_mcs.status_instapay == bs.InvDataMcs.INSTAPAY_STATUS_PENDING_SIGNER:
            signers = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signers
        
        elif invoice_mcs.status_instapay == bs.InvDataMcs.INSTAPAY_STATUS_PENDING_CHECKER_PARENT:
            if last_order == 0: # tentukan user parent
                entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            checker_parents = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checker_parents

        elif invoice_mcs.status_instapay == bs.InvDataMcs.INSTAPAY_STATUS_pENDING_SIGNER_PARENT:
            # pengecekan approval checker parent
            if last_order == 0:
                approval_to_checker = db.get_approval_ongoing(
                    invoice_id=invoice.invoice_id,
                    status=32,
                    criteria="instapay",
                    is_valid=True
                )

                if approval_to_checker:
                    entitas_id = user_id[:5]

                else:
                    entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            signer_parents = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signer_parents

        return []

    except Exception as e:
        logging.warning(traceback.format_exc())
        return None, None, None


def validate_approve_cancel(user_id, invoice, approval_ongoing):
    try:
        """
        STATUS CANCEL :
        - PENDING CHECKER = 13
        - PENDNNG SIGNER = 14
        - APPROVE = 15
        - REJECT = 16

        - PENDING CHECKER PARENT = 32
        - PENDING SIGNER PARENT = 33
        """
        invoice_mcs = invoice.detail_mcs_ongoing[0]
        last_order = invoice_mcs.last_order + 1
        if invoice_mcs.status_cancel == bs.InvDataMcs.CANCEL_STATUS_PENDING_CHECKER:
            checkers = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checkers

        elif invoice_mcs.status_cancel == bs.InvDataMcs.CANCEL_STATUS_PENDING_SIGNER:
            signers = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signers

        elif invoice_mcs.status_cancel == bs.InvDataMcs.CANCEL_STATUS_PENDING_CHECKER_PARENT:
            if last_order == 0: # tentukan user parent
                entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            checker_parents = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checker_parents

        elif invoice_mcs.status_cancel == bs.InvDataMcs.CANCEL_STATUS_PENDING_SIGNER_PARENT:
            # pengecekan approval checker parent
            if last_order == 0:
                approval_to_checker = db.get_approval_ongoing(
                    invoice_id=invoice.invoice_id,
                    status=32,
                    criteria="cancel",
                    is_valid=True
                )

                if approval_to_checker:
                    entitas_id = user_id[:5]

                else:
                    entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            signer_parents = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signer_parents

        return []

    except Exception as e:
        logging.warning(traceback.format_exc())
        return None, None, None


def validate_approve_advancesettlement(user_id, invoice, approval_ongoing):
    try:
        """
        STATUS ADVANCE SETTLEMENT :
        - PENDING CHECKER = 21
        - PENDING SIGNER = 22
        - APPROVE = 23
        - REJECT = 24

        - PENDING CHECKER PARENT = 32
        - PENDING SIGNER PARENT = 33
        """
        
        invoice_mcs = invoice.detail_mcs[0]
        last_order = invoice_mcs.last_order + 1
        if invoice_mcs.status_advset == bs.InvDataMcs.ADVSET_STATUS_PENDING_CHECKER:
            checkers = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checkers

        elif invoice_mcs.status_advset == bs.InvDataMcs.ADVSET_STATUS_PENDING_SIGNER:
            signers = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signers

        elif invoice_mcs.status_advset == bs.InvDataMcs.ADVSET_STATUS_PENDING_CHECKER_PARENT:
            if last_order == 0: # tentukan user parent
                entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            checker_parents = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checker_parents

        elif invoice_mcs.status_advset == bs.InvDataMcs.ADVSET_STATUS_PENDING_SIGNER_PARENT:
            # pengecekan approval checker parent
            if last_order == 0:
                approval_to_checker = db.get_approval_ongoing(
                    invoice_id=invoice.invoice_id,
                    status=32,
                    criteria="advset",
                    is_valid=True
                )

                if approval_to_checker:
                    entitas_id = user_id[:5]

                else:
                    entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            signer_parents = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signer_parents

    except Exception as e:
        logging.warning(traceback.format_exc())
        return None, None, None


def validate_approve_invoice(user_id, invoice, approval_ongoing):
    """
    STATUS APPROVAL INVOICE:
    - PENDING CHECKER = 2
    - PENDING SIGNER = 3
    - PENDING CHECKER CONFIRM = 6
    - PENDING SIGNER CONFIRM = 8
    - APPROVE = 8
    - REJECT = 4

    - PENDING CHECKER PARENT = 32
    - PENDING SIGNER PARENT = 33
    - PENDING CHECKER PARENT CONFIRM = 35
    - PENDING SIGNER PARENT CONFIRM = 36
    """

    invoice_mcs = invoice.detail_mcs[0]
    last_order = invoice_mcs.last_order + 1

    if invoice_mcs.status != bs.InvDataMcs.STATUS_APPROVED: # untuk approval
        if invoice_mcs.status == bs.InvDataMcs.STATUS_PENDING_APPROVE_CHECKER:
            checkers = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checkers

        elif invoice_mcs.status == bs.InvDataMcs.STATUS_PENDING_APPROVE_SIGNER:
            signers = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signers

        elif invoice_mcs.status == bs.InvDataMcs.STATUS_PENDING_CHECKER_PARENT:
            if last_order == 0: # tentukan user parent
                entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            checker_parents = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checker_parents

        elif invoice_mcs.status == bs.InvDataMcs.STATUS_PENDING_SIGNER_PARENT:
            # pengecekan approval checker parent
            if last_order == 0:
                approval_to_checker = db.get_approval_ongoing(
                    invoice_id=invoice.invoice_id,
                    status=32,
                    multiple_criteria=["approve", "confirm"],
                    is_valid=True
                )

                if approval_to_checker:
                    entitas_id = user_id[:5]

                else:
                    entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            signer_parents = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signer_parents

    else: # untuk confirmation
        if invoice_mcs.status_confirm == bs.InvDataMcs.STATUS_CONFIRMATION_PENDING_CHECKER:
            checkers = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checkers

        elif invoice_mcs.status_confirm == bs.InvDataMcs.STATUS_CONFIRMATION_PENDING_SIGNER:
            signers = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=user_id[:5],
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signers

        elif invoice_mcs.status_confirm == bs.INvDataMcs.STATUS_PENDING_CHECKER_PARENT_CONFIRM:
            if last_order == 0: # tentukan user parent
                entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            checker_parents = db.get_mst_map_kewenangan(
                role_id=4,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return checker_parents

        elif invoice_mcs.status_confirm == bs.InvDataMcs.STATUS_PENDING_SIGNER_PARENT_CONFIRM:
            # pengecekan approval checker parent
            if last_order == 0:
                approval_to_checker = db.get_approval_ongoing(
                    invoice_id=invoice.invoice_id,
                    status=35,
                    multiple_criteria=["approve", "confirm"],
                    is_valid=True
                )

                if approval_to_checker:
                    entitas_id = user_id[:5]

                else:
                    entitas_id = db.get_detail_entitas(entitas_id=user_id[:5]).parent_id

            else:
                entitas_id = user_id[:5]

            signer_parents = db.get_mst_map_kewenangan(
                role_id=3,
                entitas_id=entitas_id,
                mcs_type=invoice.mcs_type,
                group_name_alias=invoice.group_name,
                action_order=last_order,
                group_by_query="user_id",
                fitur_id=invoice.payment_method,
                user_id_out=user_id
            )

            return signer_parents


def get_user_maker(entitas_id: str = None, payment_method: str = None, mcs_type: str = None, group_name: str = None, amount_invoice: int = None):
    users_maker = db.get_mst_map_kewenangan(
        role_id=5,
        entitas_id=entitas_id,
        fitur_id=payment_method,
        amount_invoice=amount_invoice,
        mcs_type=mcs_type,
        group_name_alias=group_name,
        group_by_query="user_id"
    )

    return users_maker

