import sys
import datetime

import logging
from config.constant import EmailnotifConfig


def int_to_money(curr_prefix: str = "Rp.", amount: float = None):
    if amount:
        result = curr_prefix + str(("{:_.2f}".format(amount).replace(",", ".").replace("_",",")))
        return result

    else:
        return str(curr_prefix + "0,00")


def ymd_to_dmy(date_str):
    return datetime.datetime.strptime(date_str, "%Y-%m-%d").strftime("%d-%m-%Y")


def ymdhis_to_dmyhis(date_str: str):
    return datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S').strftime('%d-%m-%Y %H:%M:%S')
    

def calc_delta_date(start_date: str, end_date: str, start_format: str = "%Y-%m-%d", end_format: str = "%Y-%m-%d"):
    deltatime = datetime.datetime.strptime(end_date, end_format) - datetime.datetime.strptime(start_date, start_format)
    return str(deltatime.days)


def generate_email(template_dir, email_params):
    try:
        logging.info(f'File template location : {EmailnotifConfig.WORKDIR+template_dir}')
        with open(EmailnotifConfig.WORKDIR+template_dir, encoding='utf-8') as html:
            message = html.read().format(**email_params)

        return message

    except Exception as e:
        logging.error("Failed generate email message.", exc_info=True)