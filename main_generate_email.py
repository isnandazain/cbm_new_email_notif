import os
import json
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
import time
import traceback

import pika

from config.constant import EmailnotifConfig
import controllers.advance_settlement as ctrl_advset
import controllers.approve_invoice as ctrl_appinvoice
import controllers.cancel_invoice as ctrl_cancel
import controllers.instapay as ctrl_instapay
import controllers.invoice_payment as ctrl_invpay

import controllers.disbursement as ctrl_disbursement
import controllers.settlement as ctrl_settlement
import controllers.reject_invoice as ctrl_rejectinvoice


# logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s")
filename = "log/generate_email.log"
filelog = logging.handlers.TimedRotatingFileHandler(filename=filename, when="midnight", backupCount=30)
filelog.setFormatter(logging.Formatter("[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s"))

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(filelog)


def callback(ch, method, properties, body):
    try:
        logging.info(body)
        data = json.loads(body)
        notification_type = data["notification_type"]
        message_data = data["message_data"]

        invoice_id, batch_id = None, None
        if "invoice_id" in data.keys():
            invoice_id = data["invoice_id"]

        if "batch_id" in data.keys():
            batch_id = data["batch_id"]

        # ack message
        ch.basic_ack(delivery_tag=method.delivery_tag, multiple=False)

        # pendefinisian data request
        invoice_id = message_data["invoice_id"] if "invoice_id" in message_data.keys() else None
        user_id = message_data["user_id"] if "user_id" in message_data.keys() else None
        payment_method = message_data["payment_method"] if "payment_method" in message_data.keys() else None

        func_dict = {
            "CREATEINVOICE": ctrl_appinvoice.process_actual_createinvoice,
            "ACTUALDISBURSEMENT": ctrl_disbursement.process_actual_disbursement,
            "ACTUALSETTLEMENT": ctrl_settlement.process_actual_settlement,
            "INVOICEPAYMENT": ctrl_invpay.process_actual_invoicepayment,
            "ACTUALINSTAPAY": ctrl_instapay.process_actual_instapay,
            "ACTUALCANCEL": ctrl_cancel.process_actual_cancel,
            "FACILITYPAYMENT": ctrl_advset.process_actual_advancesettlement,
            "APPROVEINVOICE": ctrl_appinvoice.process_approval_invoice,
            "APPROVEINVPAY": ctrl_invpay.process_approve_invoicepayment,
            "CANCELINVOICE": ctrl_cancel.process_approve_cancel,
            "APPROVEFACPAY": ctrl_advset.process_approve_advancesettlement,
            "APPROVEINSTAPAY": ctrl_instapay.process_approve_instapay,
            "MAKEINVOICE": ctrl_appinvoice.process_approval_invoice,
            "REWORKINVOICE": ctrl_rejectinvoice.process_rework_invoice,
            "REVISIONINVOICE": ctrl_rejectinvoice.process_revision_invoice
        }

        """
        Catatan - Penerima :
        - Create Invoice = Anchor, Partner, Parent (jika ada)
        - Disbursement = Anchor, Partner
        - Settlement = {
            "DF" => Partner
            "VF" => Anchor, Partner
        }
        - Invoice Payment = Partner
        - Instapay = Anchor
        - 
        """

        try:
            response = func_dict[notification_type](
                invoice_id=invoice_id,
                user_id=user_id,
                payment_method=payment_method
            )

        except Exception as e:
            logging.warning(traceback.format_exc())
            logging.warning("Fitur belum terdaftar")


    except Exception as e:
        logging.warning(traceback.format_exc())
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)



if __name__ == "__main__":
    os.makedirs(EmailnotifConfig.LOGDIR + "log", exist_ok=True)
    rabbit_credentials = pika.PlainCredentials(username=EmailnotifConfig.USERMQ, password=EmailnotifConfig.PASSMQ)
    with pika.BlockingConnection(pika.ConnectionParameters(host=EmailnotifConfig.IPMQ, port=EmailnotifConfig.PORTMQ, virtual_host=EmailnotifConfig.VHOSTMQ, credentials=rabbit_credentials)) as connection:
        with connection.channel() as channel:
            channel.exchange_declare(
                exchange=EmailnotifConfig.EXCHANGE_NAME,
                exchange_type="direct",
                durable=True
            )
            channel.queue_declare(queue=EmailnotifConfig.QUEUE_GENERATE, durable=True)
            channel.queue_bind(queue=EmailnotifConfig.QUEUE_GENERATE, exchange=EmailnotifConfig.EXCHANGE_NAME, routing_key=EmailnotifConfig.QUEUE_GENERATE)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(
                queue=EmailnotifConfig.QUEUE_GENERATE,
                on_message_callback=callback,
                auto_ack=False
            )

            try:
                channel.start_consuming()

            except KeyboardInterrupt:
                channel.close()
                connection.close()

