import sys
import time
import json

import datetime
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
import requests
import traceback

import htmlmin
import secrets
import numpy as np
import queue
import concurrent.futures

import helper.database as db
import libs.formatter as fmt

from config.constant import EmailnotifConfig

# logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s")
filename = "log/pendingemail.log"
filelog = logging.handlers.TimedRotatingFileHandler(filename=filename, when="midnight", backupCount=30)
filelog.setFormatter(logging.Formatter("[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s"))

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(filelog)


def process_pending_email(fitur: str = None , max_retry: int = 2):
    email_pendings = db.get_logged_email(
        type_email=fitur,
        respon_status=max_retry
    )

    logging.info("Total Pending Email : " + str(len(email_pendings)))
    for email_pending in email_pendings:
        logging.info("id_email : " + str(email_pending.id_email))
        recipient = email_pending.to_email
        subject = email_pending.subject
        message = email_pending.content
        response_status = email_pending.respon_status
        email_id = email_pending.id_email

        # start send email
        sender = EmailnotifConfig.SERV_EMAIL_SENDER
        password = EmailnotifConfig.SERV_EMAIL_PASSWORD

        min_message = htmlmin.minify(message)
        subject_email = f'{subject} #ID{str(email_id[:5])}'

        try:
            url = EmailnotifConfig.SERV_EMAIL_URL
            headers = {
                "Content-Type": "application/json",
                "Postman-Token": "4b998144-4765-4a9c-adbe-3cc5bc0763ef",
                "cache-control": 'no-cache'
            }

            request_data = {
                "email": sender,
                "password": password,
                "email_name": EmailnotifConfig.SERV_EMAIL_NAME,
                "to_address": recipient,
                "cc_address": "",
                "subject": subject_email,
                "message": min_message
            }

            response = requests.post(url, data=json.dumps(request_data), headers=headers, timeout=10)
            if response.status_code == 200:
                response = response.json()
                if response["responseCode"] == "00":
                    logging.info("Re-send email success")
                    response_status = 0
                
                else:
                    logging.warning("Re-send email failed")
                    response_status += 1

            else:
                logging.warning("Re-send email failed")
                response_status += 1

        except Exception as e:
            logging.warning(traceback.format_exc())
            logging.warning("Error request send_email : " + str(e))

        # process update data email_log
        db.update_log_email(id_email=email_id, to_respon_status=response_status)

    return True
    

if __name__ == "__main__":
    PATH = EmailnotifConfig.LOGDIR
    list_fitur = EmailnotifConfig.FEATURES_LIST

    for fitur in list_fitur:
        try:
            logging.info("Start Pending Email Fitur : " + str(fitur))
            process_pending_email(fitur=fitur, max_retry=5)

        except Exception as e:
            logging.warning(traceback.format_exc())
            logging.warning("Error Pending Email : " + str(e))

