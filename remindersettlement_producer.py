import os
import sys
import time
import json

import datetime
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
import requests
import traceback

import htmlmin
import secrets
import numpy as np
import queue
import concurrent.futures

import helper.database as db
import libs.formatter as fmt

from config.constant import EmailnotifConfig

# logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s")
filename = "log/reminder_settlement.log"
filelog = logging.handlers.TimedRotatingFileHandler(filename=filename, when="midnight", backupCount=30)
filelog.setFormatter(logging.Formatter("[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s"))

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(filelog)


def request_send_email(recipient, subject, message):
    try:
        url = EmailnotifConfig.SERV_EMAIL_URL
        headers = {
            "Content-Type": "application/json",
            "Postman-Token": "4b998144-4765-4a9c-adbe-3cc5bc0763ef",
            "cache-control": 'no-cache'
        }

        request_data = {
            "email": EmailnotifConfig.SERV_EMAIL_SENDER,
            "password": EmailnotifConfig.SERV_EMAIL_PASSWORD,
            "email_name": EmailnotifConfig.SERV_EMAIL_NAME,
            "to_address": recipient,
            "cc_address": "",
            "subject": subject,
            "message": message
        }

        response = requests.post(url, data=json.dumps(request_data), headers=headers, timeout=5)
        if response.status_code == 200:
            response = response.json()
            if response["responseCode"] == "00":
                return {
                    "respon_status": 0,
                    "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
                }

            else:
                return {
                    "respon_status": 1,
                    "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
                }

        else:
            logging.warning("Stop invoice email failed")
            return {
                "respon_status": 1,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }
            
    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error request stop remind_settlement : " + str(e))
        return {
            "respon_status": 1,
            "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
        }


def process_reminder_settlement(invoice, recipient_entitas_id, payment_method, type_reminder):
    amount = fmt.int_to_money(amount=float(invoice.amount))
    settlement_date = invoice.maturity_date
    actual_disburse_date = invoice.detail_mcs[0].disbursement_date

    today = datetime.date.today().strftime("%Y-%m-%d")
    remind_days = fmt.calc_delta_date(
        end_date=invoice.maturity_date.strftime("%Y-%m-%d"),
        start_date=today,
        end_format="%Y-%m-%d",
        start_format="%Y-%m-%d"
    )

    seller = invoice.entitas_partner_name if invoice.payment_method == "VF" else invoice.entitas_anchor_name
    buyer = invoice.entitas_anchor_name if invoice.payment_method == "DF" else invoice.entitas_partner_name

    if recipient_entitas_id == invoice.entitas_anchor:
        users = db.get_email_entitas(entitas_id=recipient_entitas_id, payment_method=payment_method, get_from="anchor")
        users = users[0].email_anchor if users else []
        users = users.split("|") if users else []

    elif recipient_entitas_id == invoice.entitas_partner:
        users = db.get_email_entitas(entitas_id=invoice.entitas_anchor, partner_id=recipient_entitas_id, payment_method=payment_method, get_from="partner")
        users = users[0].email_partner if users else []
        users = users.split("|") if users else []

    list_data_log = []
    for user in users:
        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Settlement Reminder " + invoice.invoice_id
        if invoice.payment_method == "DF":
            template_dir = "resources/email_template/df/reminder_settlement_template.html"
            email_params = {
                "email": str(user),
                "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
                "payment_method": invoice.payment_method,
                "invoice_id": invoice.invoice_id,
                "seller": seller,
                "buyer": buyer,
                "settlement_account": invoice.account_settlement,
                "loan_account": invoice.account_disbursement,
                "amount": amount,
                "disburse_date": actual_disburse_date,
                "settle_date": settlement_date,
                "remind_days": str(remind_days) + " day" if remind_days <= "1" else str(remind_days) + " days"
            }

        elif invoice.payment_method == "VF":
            template_dir = "resources/email_template/vf/reminder_settlement_template.html"
            email_params = {
                "email": str(user),
                "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
                "payment_method": invoice.payment_method,
                "invoice_id": invoice.invoice_id,
                "seller": seller,
                "buyer": buyer,
                "settlement_account": invoice.account_settlement,
                "loan_account": invoice.detail_vf[0].loan_vendor,
                "amount": amount,
                "disburse_date": actual_disburse_date,
                "settle_date": settlement_date,
                "remind_days": str(remind_days) + " day" if remind_days <= "1" else str(remind_days) + " days"
            }

        message = fmt.generate_email(template_dir, email_params)
        min_message = htmlmin.minify(message)
        recipient = user

        # generate email_id
        email_id = None
        id_email_exist = True
        while id_email_exist:
            email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
            log_email_exist = db.get_logged_email(id_email=email_id)
            id_email_exist = True if log_email_exist else False

        # request data
        data_log = {
            "id_email": email_id,
            "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
            "fitur": type_reminder,
            "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
            "to_email": recipient,
            "subject": subject,
            "content": min_message,
            "respon_status": 6,
            "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
        }

        # insert log
        db.insert_log_email(
            id_email=email_id,
            send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
            fitur=type_reminder,
            from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
            to_email=recipient,
            subject=subject,
            content=min_message,
            respon_status=6,
            respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
        )

        # tampung data log
        list_data_log.append(data_log)

    # generate request ke bri-notification
    response_service = {}
    section = int(len(list_data_log) / 4) + 1
    split_data_log = np.array_split(list_data_log, section)
    for sp_data_log in split_data_log:
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            # start a future for a thread
            request_data_collection = {}
            for index, data in enumerate(sp_data_log):
                request_data_collection[executor.submit(
                    request_send_email,
                    recipient=data["to_email"],
                    subject=data["subject"],
                    message=data["content"]
                )] = data["id_email"]

            while request_data_collection:
                done, not_done = concurrent.futures.wait(
                    request_data_collection, timeout=5,
                    return_when=concurrent.futures.FIRST_COMPLETED
                )

                for future in done:
                    index = request_data_collection[future]

                    try:
                        data = future.result()
                    except Exception as exc:
                        logging.warning("%i generated an exception: %s", (index, exc))
                    else:
                        response_service[index] = data

                    del request_data_collection[future]

    # update data log berdasarkan response service
    for index, data in enumerate(list_data_log):
        result_send_email = response_service[data["id_email"]]
        db.update_log_email(
            id_email=data["id_email"], 
            to_respon_status=result_send_email["respon_status"], 
            to_respon_date=result_send_email["respon_date"]
        )

    return None


if __name__ == "__main__":
    PATH = EmailnotifConfig.LOGDIR

    try:
        logging.info("Start Reminder Settlement H-7 Producer!!")
        invoice_vf_7days = db.get_reminder_invoices(payment_method="VF", h_minus=7)
        invoice_df_7days = db.get_reminder_invoices(payment_method="DF", h_minus=7)

        logging.info("Total invoice VF : " + str(len(invoice_vf_7days)))
        logging.info("Total invoice DF : " + str(len(invoice_df_7days)))

        for invoice in invoice_vf_7days:
            if not db.get_logged_email(type_email=EmailnotifConfig.REMINDER_SETTLEMENT_7DAYS, invoice_id=invoice.invoice_id):
                process_reminder_settlement(
                    invoice=invoice, 
                    recipient_entitas_id=invoice.entitas_anchor, 
                    payment_method=invoice.payment_method, 
                    type_reminder=EmailnotifConfig.REMINDER_SETTLEMENT_7DAYS
                )

                process_reminder_settlement(
                    invoice=invoice,
                    recipient_entitas_id=invoice.entitas_partner,
                    payment_method=invoice.payment_method,
                    type_reminder=EmailnotifConfig.REMINDER_SETTLEMENT_7DAYS
                )

        for invoice in invoice_df_7days:
            if not db.get_logged_email(type_email=EmailnotifConfig.REMINDER_SETTLEMENT_1DAYS, invoice_id=invoice.invoice_id):
                process_reminder_settlement(
                    invoice=invoice, 
                    recipient_entitas_id=invoice.entitas_partner, 
                    payment_method=invoice.payment_method, 
                    type_reminder=EmailnotifConfig.REMINDER_SETTLEMENT_7DAYS
                )
            
        logging.info("Start Reminder Settlement H-1 Producer")
        invoice_vf_1days = db.get_reminder_invoices(payment_method="VF", h_minus=1)
        invoice_df_1days = db.get_reminder_invoices(payment_method="DF", h_minus=1)

        logging.info("Total invoice VF : " + str(len(invoice_vf_1days)))
        logging.info("Total invoice DF : " + str(len(invoice_df_1days)))

        for invoice in invoice_vf_1days:
            if not db.get_logged_email(type_email=EmailnotifConfig.REMINDER_SETTLEMENT_1DAYS, invoice_id=invoice.invoice_id):
                process_reminder_settlement(
                    invoice=invoice, 
                    recipient_entitas_id=invoice.entitas_anchor, 
                    payment_method=invoice.payment_method, 
                    type_reminder=EmailnotifConfig.REMINDER_SETTLEMENT_1DAYS
                )

                process_reminder_settlement(
                    invoice=invoice, 
                    recipient_entitas_id=invoice.entitas_partner, 
                    payment_method=invoice.payment_method, 
                    type_reminder=EmailnotifConfig.REMINDER_SETTLEMENT_1DAYS
                )

        for invoice in invoice_df_1days:
            if not db.get_logged_email(type_email=EmailnotifConfig.REMINDER_SETTLEMENT_1DAYS, invoice_id=invoice.invoice_id):
                process_reminder_settlement(
                    invoice=invoice, 
                    recipient_entitas_id=invoice.entitas_partner, 
                    payment_method=invoice.payment_method, 
                    type_reminder=EmailnotifConfig.REMINDER_SETTLEMENT_1DAYS
                )


    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error : " + str(e))