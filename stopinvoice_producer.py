import os
import sys
import json
import time
import datetime

import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
import traceback
import requests
import secrets

import htmlmin
import numpy as np
import queue
import concurrent.futures

import helper.database as db
import libs.formatter as fmt

from config.constant import EmailnotifConfig

# logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s")
filename = "log/stopinvoice.log"
filelog = logging.handlers.TimedRotatingFileHandler(filename=filename, when="midnight", backupCount=30)
filelog.setFormatter(logging.Formatter("[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s"))

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(filelog)


def request_send_email(recipient, subject, message):
    try:
        url = EmailnotifConfig.SERV_EMAIL_URL
        headers = {
            "Content-Type": "application/json",
            "Postman-Token": "4b998144-4765-4a9c-adbe-3cc5bc0763ef",
            "cache-control": 'no-cache'
        }

        request_data = {
            "email": EmailnotifConfig.SERV_EMAIL_SENDER,
            "password": EmailnotifConfig.SERV_EMAIL_PASSWORD,
            "email_name": EmailnotifConfig.SERV_EMAIL_NAME,
            "to_address": recipient,
            "cc_address": "",
            "subject": subject,
            "message": message
        }

        response = requests.post(url, data=json.dumps(request_data), headers=headers, timeout=5)
        if response.status_code == 200:
            response = response.json()
            if response["responseCode"] == "00":
                return {
                    "respon_status": 0,
                    "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
                }

            else:
                return {
                    "respon_status": 1,
                    "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
                }

        else:
            logging.warning("Stop invoice email failed")
            return {
                "respon_status": 1,
                "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
            }
            
    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error request stop remind_settlement : " + str(e))
        return {
            "respon_status": 1,
            "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
        }



def process_stop_invoice(invoice, recipient_entitas_id, payment_method, anchor_id=None):
    amount = fmt.int_to_money(amount=float(invoice.amount))
    type_reminder = EmailnotifConfig.STOPINVOICE
    
    invoice_mcs = invoice.detail_mcs[0]
    seller = invoice.entitas_partner_name if invoice.payment_method == "VF" else invoice.entitas_anchor_name
    buyer = invoice.entitas_anchor_name if invoice.payment_method == "VF" else invoice.entitas_partner_name

    actual_disburse_date = invoice_mcs.disbursement_date
    maturity_date = invoice.maturity_date

    # generate message content
    message_content = ""
    message_content_en = ""

    if invoice.payment_method == "VF":
        message_content = "Keterlambatan pembayaran mengakibatkan <i>stop invoice</i>. " \
                          "Demi kenyamanan bertransaksi, mohon segera melakukan pembayaran pada kesempatan pertama."
        message_content_en = "Late payment may result stop invoice. " \
                             "For the convenience of transactions, please make payments at the first opportunity."

        company_role = "entitas_anchor" if invoice.entitas_anchor == recipient_entitas_id else "entitas_partner"
        if company_role == "entitas_anchor":
            # get user
            users = db.get_email_entitas(entitas_id=recipient_entitas_id, payment_method=payment_method, get_from="anchor")
            users = users[0].email_anchor.split("|") if users else []

        elif company_role == "entitas_partner":
            # get user
            users = db.get_email_entitas(entitas_id=invoice.entitas_anchor, partner_id=recipient_entitas_id, payment_method=payment_method, get_from="partner")
            users = users[0].email_partner.split("|") if users else []

    elif invoice.payment_method == "DF":
        company_role = "entitas_anchor" if invoice.entitas_anchor == recipient_entitas_id else "entitas_partner"
        if str(company_role) == "entitas_anchor":
            message_content = "Mohon bantuan untuk melakukan <i>stop invoice</i> terhadap pemesanan dari distributor " \
                              "di atas sampai dengan pembayaran telah dilakukan."
            message_content_en = "Please do a stop invoice from the distributor above " \
                                 "until the payment has been made."

            # get user
            users = db.get_email_entitas(entitas_id=recipient_entitas_id, payment_method=payment_method, get_from="anchor")
            users = users[0].email_anchor.split("|") if users else []

        elif str(company_role) == "entitas_partner":
            message_content = "Keterlambatan pembayaran mengakibatkan stop invoice dari <i>seller</i>. " \
                              "Demi kenyamanan bertransaksi, mohon segera melakukan pembayaran pada kesempatan pertama."
            message_content_en = "Late payment may result stop invoice from seller. " \
                                 "For the convenience of transactions, please make payments at the first opportunity."

            # get user
            users = db.get_email_entitas(entitas_id=invoice.entitas_anchor, partner_id=recipient_entitas_id, payment_method=payment_method, get_from="partner")
            users = users[0].email_partner.split("|") if users else []

    # generate email params
    list_data_log = []
    for user in users:
        email_params = {
            "email": str(user),
            "reference_id": invoice.entitas_inv_no.replace("-", "&#8209;"),
            "payment_method": invoice.payment_method,
            "invoice_id": invoice.invoice_id,
            "seller": seller,
            "buyer": buyer,
            "loan_account": invoice.account_disbursement if invoice.payment_method == "DF" else invoice.loan_vendor,
            "amount": amount,
            "disburse_date": actual_disburse_date,
            "settle_date": maturity_date,
            "payment_status": "OVERDUE",
            "message_content": message_content,
            "message_content_en": message_content_en
        }

        subject = EmailnotifConfig.SERV_EMAIL_SUBJECT + "Overdue & Stop Invoice " + invoice.invoice_id
        template_dir = "resources/email_template/stop_invoice_template.html"
        message = fmt.generate_email(template_dir , email_params)
        min_message = htmlmin.minify(message)

        recipient = user

        # generate email_id
        email_id = None
        id_email_exist = True
        while id_email_exist:
            email_id = str(secrets.randbelow(9999)) + time.strftime("%Y%m%d%H%M%S")
            log_email_exist = db.get_logged_email(id_email=email_id)
            id_email_exist = True if log_email_exist else False

        # data untuk email_log
        data_log = {
            "id_email": email_id,
            "send_date": time.strftime("%Y-%m-%d %H:%M:%S"),
            "fitur": "STOPINVOICE",
            "from_email": EmailnotifConfig.SERV_EMAIL_SENDER,
            "to_email": recipient,
            "subject": subject,
            "content": min_message,
            "respon_status": 6,
            "respon_date": time.strftime("%Y-%m-%d %H:%M:%S")
        }

        # insert log
        db.insert_log_email(
            id_email=email_id,
            send_date=time.strftime("%Y-%m-%d %H:%M:%S"),
            fitur=type_reminder,
            from_email=EmailnotifConfig.SERV_EMAIL_SENDER,
            to_email=recipient,
            subject=subject,
            content=min_message,
            respon_status=6,
            respon_date=time.strftime("%Y-%m-%d %H:%M:%S")
        )

        # tampung data log
        list_data_log.append(data_log)


    # generate request ke bri-notification
    response_service = {}
    section = int(len(list_data_log) / 4) + 1
    split_data_log = np.array_split(list_data_log, section)
    for sp_data_log in split_data_log:
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            # start a future for a thread
            request_data_collection = {}
            for index, data in enumerate(sp_data_log):
                request_data_collection[executor.submit(
                    request_send_email,
                    recipient=data["to_email"],
                    subject=data["subject"],
                    message=data["content"]
                )] = data["id_email"]

            while request_data_collection:
                done, not_done = concurrent.futures.wait(
                    request_data_collection, timeout=5,
                    return_when=concurrent.futures.FIRST_COMPLETED
                )

                for future in done:
                    index = request_data_collection[future]

                    try:
                        data = future.result()
                    except Exception as exc:
                        logging.warning("%i generated an exception: %s", (index, exc))
                    else:
                        response_service[index] = data

                    del request_data_collection[future]

    # update data log berdasarkan response service
    for index, data in enumerate(list_data_log):
        result_send_email = response_service[data["id_email"]]
        db.update_log_email(
            id_email=data["id_email"], 
            to_respon_status=result_send_email["respon_status"], 
            to_respon_date=result_send_email["respon_date"]
        )

    return None


if __name__ == "__main__":
    PATH = EmailnotifConfig.LOGDIR

    try:
        logging.info("Start Stop Invoice Producer")
        stop_invoice_vf = db.get_stop_invoices(payment_method="VF", interval=1)
        stop_invoice_df = db.get_stop_invoices(payment_method="DF", interval=1)
        logging.info("Total Invoice VF : " + str(len(stop_invoice_vf)))
        logging.info("Total Invoice DF : " + str(len(stop_invoice_df)))
        
        # untuk VF
        for invoice in stop_invoice_vf:
            if not db.get_logged_email(type_email=EmailnotifConfig.STOPINVOICE, invoice_id=invoice.invoice_id):
                process_stop_invoice(
                    invoice=invoice,
                    recipient_entitas_id=invoice.entitas_anchor,
                    payment_method="VF"
                )

                process_stop_invoice(
                    invoice=invoice,
                    recipient_entitas_id=invoice.entitas_partner,
                    payment_method="VF",
                    anchor_id=invoice.entitas_anchor
                )

        # untuk DF
        for invoice in stop_invoice_df:
            if not db.get_logged_email(type_email=EmailnotifConfig.STOPINVOICE, invoice_id=invoice.invoice_id):
                process_stop_invoice(
                    invoice=invoice,
                    recipient_entitas_id=invoice.entitas_anchor,
                    payment_method="DF"
                )

                process_stop_invoice(
                    invoice=invoice,
                    recipient_entitas_id=invoice.entitas_partner,
                    payment_method="DF",
                    anchor_id=invoice.entitas_anchor
                )


    except Exception as e:
        logging.warning(traceback.format_exc())
        logging.warning("Error stop_invoice : " + str(e))
